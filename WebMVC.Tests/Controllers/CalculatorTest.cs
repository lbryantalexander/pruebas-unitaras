﻿using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMVC.Tests.Controllers
{

    [TestClass]
    public class CalculatorTest
    {
        
        [TestInitialize]
        public void OnTestInitialize()
        {
            _SystemUnderTest = null;
        }

        private Calculator _SystemUnderTest;

        public Calculator SystemUnderTest
        {
            get
            {
                if(_SystemUnderTest == null)
                {
                    _SystemUnderTest = new Calculator();
                }
                return _SystemUnderTest;
            }


        }

        [TestMethod]
        public void Add()
        {

            //Arrange (Organizar)
            int value1 = 3;
            int value2 = 3;
            int expected = 6;

            //Act (Actuar)


            int actual = SystemUnderTest.Add(value1,value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }

        [TestMethod]
        public void Add3()
        {

            //Arrange (Organizar)
            int value1 = 10;
            int value2 = 2;
            int expected = 5;

            //Act (Actuar)


            int actual = SystemUnderTest.Add3(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }

        [TestMethod]
        public void Add2()
        {

            //Arrange (Organizar)
            int value1 = 2;
            int value2 = 3;
            int expected = 6;

            //Act (Actuar)


            int actual = SystemUnderTest.Add2(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }

        [TestMethod]
        public void Add4()
        {

            //Arrange (Organizar)
            int value1 = 15;
            int value2 = 5;
            int expected = 10;

            //Act (Actuar)


            int actual = SystemUnderTest.Add4(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }

    }
}
