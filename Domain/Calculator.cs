﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Calculator
    {
        public int Add(int value1, int value2)
        {
            return value1 + value2; 
        }

        public int Add2(int value1, int value2)
        {
            return value1 * value2;
        }

        public int Add3(int value1, int value2)
        {
            return value1 / value2;
        }

        public int Add4(int value1, int value2)
        {
            return value1 - value2;
        }
    }
}
